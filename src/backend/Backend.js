import { Socket } from 'net';

export default class Backend {
  constructor(address, port) {
    this.socket = new Socket();
    this.socket.on('error', (e) => {
      console.log(e);
    });
    this.socket.on('close', () => {
      console.log('close');
    });
    this.socket.connect(port, address);
  }

  sendMessage(message) {
    this.socket.write(message);
  }
}
